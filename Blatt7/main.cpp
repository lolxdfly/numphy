#include <iostream>
#include <fstream>
#include <math.h>
#include <functional>

// external forces (already divided by mass)
double Fex0(double w, double t)
{
    return 0;
}
double Fex1(double w, double t)
{
    return 1;
}
double Fex2(double w, double t)
{
    return std::sin(2 * w * t);
}
double Fex3(double w, double t)
{
    return std::sin(w * t);
}

void simSingleOscillator(std::ofstream& output)
{
    // parameters
    const double T = 60;
    const double h = 0.01;
    const double w = 1.0;
    const double y = 0.1;
    const double x0 = 0;
    const double v0 = 0;
    const std::function<double(double, double)> Fex = Fex3;

    double x = x0;
    double xn = 0;
    double v = v0;
    double vn = 0;

    for(double t = 0; t < T; t += h)
    {
        // x' = v
        xn = x + h * v;

        // v' = -y * v - w^2 * x + Fex / m
        vn = v + h * (-y * v - w * w * x + Fex(w, t));

        x = xn;
        v = vn;
        output << t << '\t' << x << '\t' << v << std::endl;
    }
}

void simSingleOscillatorRK2(std::ofstream& output)
{
    // parameters
    const double T = 60;
    const double h = 0.01;
    const double w = 1.0;
    const double y = 0.1;
    const double x0 = 0;
    const double v0 = 0;
    const std::function<double(double, double)> Fex = Fex3;

    double x = x0;
    double kx1 = 0;
    double kx2 = 0;
    double xn = 0;
    double v = v0;
    double kv1 = 0;
    double kv2 = 0;
    double vn = 0;

    for(double t = 0; t < T; t += h)
    {
        // k1 = h * f(x)
        kx1 = h * v;
        kv1 = (-y * v - w * w * x + Fex(w, t)) * h;

        // k2 = h * f(x + k1/2)
        kx2 = h * (v + (kv1 / 2.));
        kv2 = h * (-y * v - w * w * (x + (kx1 / 2.)) + Fex(w, t));

        // x = x + k2
        xn = x + kx2;
        vn = v + kv2;

        x = xn;
        v = vn;
        output << t << '\t' << x << '\t' << v << std::endl;
    }
}

void simMultiOscillator(std::ofstream& output, double wex)
{
    // parameters
    const double T = 60;
    const double h = 0.01;
    const double w1 = 1.0;
    const double w2 = 0.5;
    //const double wex = 1.0;
    const double y = 0;
    const double x10 = 0;
    const double v10 = 0;
    const double x20 = 0;
    const double v20 = 0;
    const double A = 0.5;
    const std::function<double(double, double)> Fex = Fex3;

    double x1 = x10;
    double x1n = 0;
    double v1 = v10;
    double v1n = 0;
    double x2 = x20;
    double x2n = 0;
    double v2 = v20;
    double v2n = 0;

    for(double t = 0; t < T; t += h)
    {
        // x1' = v1
        x1n = x1 + h * v1;

        // v1' = -y * v1 - w^2 * x1 + w^2(x2 - x1) + A * Fex / m
        v1n = v1 + h * (-y * v1 - w1 * w1 * x1 + w2 * w2 * (x2 - x1) + A * Fex(wex, t));

        // x2' = v2
        x2n = x2 + h * v2;

        // v2' = -y * v2 - w^2 * x2 + w^2(x2 - x1)
        v2n = v2 + h * (-y * v2 - w1 * w1 * x2 - w2 * w2 * (x2 - x1));

        x1 = x1n;
        v1 = v1n;
        x2 = x2n;
        v2 = v2n;

        /*if(x1 >= 5)
        {
            std::cout << wex << " " << x1 << std::endl;
        }*/

        output << t << '\t' << x1 << '\t' << v1  << '\t' << x2 << '\t' << v2 << std::endl;
    }
}

int main(int argc, char const *argv[])
{
    std::ofstream output;
    output.open("out.txt");

    simSingleOscillator(output);
    simSingleOscillatorRK2(output);
    //for(double d = 0; d < 5; d += 0.1)
    {
        //simMultiOscillator(output, 1.2);
    }
    // wex1 = 1; wex2 = 1.2
    
    output.close();
    return 0;
}