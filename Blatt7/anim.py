from gi.repository import Gtk
import cairo

class Renderer(Gtk.Window):

    def __init__(self):
        super(Renderer, self).__init__()
        self.data = ""
        self.t = 0
        self.init_ui()
        self.read_data()
        
    def init_ui(self):    
        darea = Gtk.DrawingArea()
        darea.connect("draw", self.on_draw)
        self.add(darea)
        self.set_title("Oscillator")
        self.resize(1024, 512)
        self.set_position(Gtk.WindowPosition.CENTER)
        self.connect("delete-event", Gtk.main_quit)
        self.show_all()

    def read_data(self):
        f = open("out.txt", "r")
        self.data = list(f.readlines())
        self.data = list(map(lambda x: x.split('\t'), self.data))
    
    def on_draw(self, wid, cr):
        # center cam
        cr.translate(512, 256)

        # render 0 line
        cr.set_source_rgb(0.6, 0, 0)
        cr.rectangle(-1, -256, 2, 512)
        cr.fill()
        
        # render box 1
        cr.set_source_rgb(0.6, 0.3, 0.6)
        x = float(self.data[self.t][1]) * 50
        cr.rectangle(x - 25, -10, 50, 20)
        cr.fill()
        #print(x)

        # render box 2
        if len(self.data[0]) > 3:
            cr.set_source_rgb(0.6, 0.6, 0.3)
            x = float(self.data[self.t][3]) * 50
            cr.rectangle(x - 25, -10, 50, 20)
            cr.fill()
            #print(x)

        # increase time
        self.t = self.t + 5
        
        # stop update when data stream ended
        if self.t < len(self.data):
            wid.queue_draw()
    
def main():
    app = Renderer()
    Gtk.main()
        
if __name__ == "__main__":    
    main()
    