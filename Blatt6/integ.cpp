#include <iostream>
#include <math.h>
#include <functional>

// typedefs
typedef std::function<double(double)> Func;
typedef std::function<double(Func, double a, double b)> Rule;

// single step rules
double rectRule(Func f, double a, double b)
{
    return (b - a) * f(a);
}
double midRule(Func f, double a, double b)
{
    return (b - a) * f((a + b) / 2.);
}
double trapRule(Func f, double a, double b)
{
    return ((b - a) / 2.) * (f(a) + f(b));
}

// improper integrals
double fimpInteg(Func f, double E)
{
    const double cE = (1. - (E * E));
    return f(E / cE) * ((1. + (E * E)) / (cE * cE)); // if E = |-1| => division by 0
}
double impInteg(Func f, Rule rule)
{
    Func newf = std::bind(fimpInteg, f, std::placeholders::_1);
    return rule(newf, -0.9999, 0.9999);
}

// multi step
double impIntegN(Func f, unsigned int N, Rule rule)
{
    double res = 0;
    const double dx = 2. / N;
    Func newf = std::bind(fimpInteg, f, std::placeholders::_1);
    for(double d = -0.9999; d < 0.9999; d += dx)
    {
        res += rule(newf, d, d + dx);
    }
    return res;
}
double integN(Func f, double a, double b, unsigned int N, Rule rule)
{
    double res = 0;
    const double dx = (b - a) / N;
    for(double d = a; d < b; d += dx)
    {
        res += rule(f, d, d + dx);
    }
    return res;
}

// test functions
double testImp(double x)
{
    return std::exp(- (x * x));
}
double test(double x)
{
    return 2. + x * x;
}
double F(double w)
{
    return std::exp(- (w * w) / 16.) / (std::pow(2., 3. / .4) * std::pow(M_PI, 1. / 4.));
}
double f(double x, unsigned int k, double w0)
{
    return std::pow((x - w0), k) * F(x);
}

void test()
{
    // single step finite
    std::cout << "single step finite rectRule: \t" << rectRule(testImp, -999, 999) << std::endl;
    std::cout << "single step finite midRule: \t" << midRule(testImp, -999, 999) << std::endl;
    std::cout << "single step finite trapRule: \t" << trapRule(testImp, -999, 999) << std::endl;

    // single step infinite
    std::cout << "single step infinite rectRule: \t" << impInteg(testImp, rectRule) << std::endl;
    std::cout << "single step infinite midRule: \t" << impInteg(testImp, midRule) << std::endl;
    std::cout << "single step infinite trapRule: \t" << impInteg(testImp, trapRule) << std::endl;

    // multi step finite
    std::cout << "multi step finite rectRule: \t" << integN(testImp, -999, 999, 100000, rectRule) << std::endl;
    std::cout << "multi step finite midRule: \t" << integN(testImp, -999, 999, 100000, midRule) << std::endl;
    std::cout << "multi step finite trapRule: \t" << integN(testImp, -999, 999, 100000, trapRule) << std::endl;

    // multi step infinite
    std::cout << "multi step infinite rectRule: \t" << impIntegN(testImp, 100000, rectRule) << std::endl;
    std::cout << "multi step infinite midRule: \t" << impIntegN(testImp, 100000, midRule) << std::endl;
    std::cout << "multi step infinite trapRule: \t" << impIntegN(testImp, 100000, trapRule) << std::endl;

    // real value
    std::cout << "Real value: sqrt(pi) = " << std::sqrt(M_PI) << std::endl;
}

int main(int argc, char const *argv[])
{
    // print with more precision
    std::cout.precision(15);

    //test();

    // task function
    Func M0 = std::bind(f, std::placeholders::_1, 0, 0.);
    Func M1 = std::bind(f, std::placeholders::_1, 1, 0.);
    std::cout << "rectRule M0(w): \t" << impIntegN(M0, 100000, rectRule) << std::endl;
    std::cout << "midRule M0(w): \t\t" << impIntegN(M0, 100000, midRule) << std::endl;
    std::cout << "trapRule M0(w): \t" << impIntegN(M0, 100000, trapRule) << std::endl;
    std::cout << "rectRule M1(w): \t" << impIntegN(M1, 100000, rectRule) << std::endl;
    std::cout << "midRule M1(w): \t\t" << impIntegN(M1, 100000, midRule) << std::endl;
    std::cout << "trapRule M1(w): \t" << impIntegN(M1, 100000, trapRule) << std::endl;

    return 0;
}