#include <iostream>
#include <fstream>
#include <math.h>
#include <functional>
#include <complex>

using namespace std::complex_literals;

double f(double t)
{
    // 4square(8 / pi) = (8 / pi)^(1 / 4)
    static const double A = std::pow(8. / M_PI, 1. / 4.);
    static const double y = 4.;

    return A * std::exp(-y * t * t);
}
double F(double w)
{
    return std::exp(- (w * w) / 16.) / (std::pow(2., 3. / .4) * std::pow(M_PI, 1. / 4.));
}

double test(double t)
{
    return std::sin(2. * M_PI * t);
}
double square(double t)
{
    if(t > -1 && t < 1)
        return 1;
    return 0;
}

double w(unsigned int q, unsigned int N, double dt)
{
    return (2. * M_PI * q) / (N * dt);
}

std::complex<double> DFT(std::function<double(double)> f, unsigned int N, double t0, double dt, unsigned int q)
{
    // 1 / sqrt(L) with L = (b - a) = N
    double pre = 1. / std::sqrt(N);

    // complex sum
    std::complex<double> sum = 0;

    // make the N complex
    const std::complex<double> zN = N;

    // sum over N
    for(unsigned int k = 0; k < N; k++)
    {
        // get f(t)
        double fk = f(t0 + k * dt);

        // make q * k complex
        std::complex<double> zqk = q * k;

        // compute element and add on sum
        sum += fk * std::exp((-2. * M_PI * 1i * zqk) / zN);
    }

    return pre * sum;
}

std::complex<double> invDFT(std::complex<double> c[], unsigned int N, double t0, double dt, unsigned int q)
{
    // 1 / sqrt(L) with L = (b - a) = N
    double pre = 1. / std::sqrt(N);

    // complex sum
    std::complex<double> sum = 0;

    // make the N complex
    const std::complex<double> zN = N;

    // sum over N
    for(unsigned int k = 0; k < N; k++)
    {
        // make q * k complex
        std::complex<double> zqk = q * k;

        // compute element and add on sum
        sum += c[k] * std::exp((2. * M_PI * 1i * zqk) / zN);
    }

    return pre * sum;
}

void testDFTinvDFT(std::ofstream& output)
{
    const unsigned int N = 1000;
    const double t0 = 0;
    const double dt = 0.001;

    // calc DFT
    std::complex<double> res[N];
    for(unsigned int q = 0; q < N; q++)
    {
        std::complex<double> dft = DFT(test, N, t0, dt, q);
        res[q] = dft;
    }

    // calc inverse DFT
    for(unsigned int q = 0; q < N; q++)
    {
        std::complex<double> invdft = invDFT(res, N, t0, dt, q);
        output << (t0 + q * dt) << '\t' << std::real<double>(invdft) << '\t' << test(t0 + q * dt) << std::endl;
    }

    // output should now contain the same values in 2:3
}

void testDFT(std::ofstream& output)
{
    const unsigned int N = 1000;
    const double t0 = 0;
    const double dt = 0.499999; // dont use 0.5, because sin(2 pi x) would be always 0

    for(unsigned int q = 0; q < N; q++)
    {
        std::complex<double> dft = DFT(test, N, t0, dt, q);
        output << w(q, N, dt) << '\t' << std::abs<double>(dft) << std::endl;
    }
}

int main(int argc, char const *argv[])
{
    std::ofstream output;
    output.open("out.txt");

    //testDFTinvDFT(output);
    //testDFT(output);

    const unsigned int N = 1000;
    const double t0 = 0; // f(-t) = f(t)
    const double dt = 0.5;

    for(unsigned int q = 0; q < N; q++)
    {
        std::complex<double> dft = DFT(f, N, t0, dt, q);
        output << w(q, N, dt) << '\t' << std::abs<double>(dft) << '\t' << F(w(q, N, dt)) << std::endl; // hmmm...
    }

    output.close();
    return 0;
}