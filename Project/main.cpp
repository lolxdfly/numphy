#include <iostream>
#include <fstream>
#include <string.h>
#include <complex>

using namespace std::complex_literals;

/**
 * @brief calculate vector matrix multiplication with dimension N
 * 
 * @tparam N dimension of vecIn, vecOut and mtrx
 * @param vecIn the input vector
 * @param mtrx the matrix
 * @param vecOut the result vector
 */
template<unsigned int N> void VecMulMtrx(double vecIn[N], double mtrx[N][N], double vecOut[N])
{
    // clear vecOut
    memset(vecOut, 0, N * sizeof(double));

    // calc matrix multiplication
    for(int i = 0; i < N; i++)
    {
        for(int j = 0; j < N; j++)
        {
            vecOut[i] += mtrx[i][j] * vecIn[j];
        }
    }
}

/**
 * @brief create matrix for N coffee mugs
 * 
 * @tparam N dimension of matrix
 * @param mtrx the result matrix
 * @param y the y coefficient
 */
template<unsigned int N>void createM(double mtrx[N][N], double y)
{
    for(unsigned int i = 0; i < N; i++)
    {
        for(unsigned int j = 0; j < N; j++)
        {
            if(i == j)
            {
                mtrx[i][j] = -2 * y;
            }
            else if(
                    (i + 1 == j || i == j + 1) ||
                    (i == (N - 1) && j == 0) ||
                    (i == 0 && j == (N - 1))
            )
            {
                mtrx[i][j] = y;
            }
            else
            {
                mtrx[i][j] = 0;
            }
        }
    }
}

/**
 * @brief calculates the discrete fourier transformation
 * 
 * @param T the input values
 * @param N 
 * @param k 
 * @return std::complex<double> the complex result
 */
std::complex<double> DFT(double T[], unsigned int N, unsigned int k)
{
    // 1 / sqrt(L) with L = (b - a) = N
    double pre = 1. / std::sqrt(N);

    // complex sum
    std::complex<double> sum = 0;

    // make the N complex
    const std::complex<double> zN = N;

    // sum over N
    for(unsigned int l = 0; l < N; l++)
    {
        // make q * l complex
        std::complex<double> zkl = k * l;

        // compute element and add on sum
        sum += T[l] * std::exp((-2. * M_PI * 1i * zkl) / zN);
    }

    return pre * sum;
}

/**
 * @brief calculates the exact solution with N = 3
 * 
 * @param Tend the time, where the simulation ends
 * @param h the step width
 * @param y the y coefficient
 * @param output the output stream for printing the results
 */
void tempSim_exact(const double Tend, const double h, const double y, std::ofstream& output)
{
    const unsigned int N = 3;
    double T[N] = {0.}; // current T

    // precalc some stuff
    const double othird = (1. / 3.);
    const double tthird = (2. / 3.);

    for(double t = 0.; t < Tend; t += h)
    {
        // e term is equal for all T
        const double e = std::exp(-3 * y * t);

        // calc T
        T[0] = othird - othird * e;
        T[1] = othird + tthird * e;
        T[2] = othird - othird * e; // = T[0]

        // print current T to out.txt
        output << t << '\t';
        for(unsigned int i = 0; i < N; i++)
        {
            output << T[i] << '\t';
        }
        output << std::endl;   
    }
}

/**
 * @brief start the temperature simulation (Euler version)
 * 
 * @tparam N the number of coffee mugs
 * @tparam useDFT flag to calculate the discrete fourier transformation
 * @param T the input temperatures
 * @param M the matrix
 * @param Tend the time, where the simulation ends
 * @param h the step width
 * @param y the y coefficient
 * @param output the output stream for printing the results
 */
template<unsigned int N, bool useDFT> void tempSim(double T[N], double M[N][N], const double Tend, const double h, const double y, std::ofstream& output)
{
    double Tderiv[N] = {0.}; // deriv T
    double Tn[N] = {0.}; // next T
    std::complex<double> Tk0[N]; // Tk(0)

    for(double t = 0.; t < Tend; t += h)
    {
        // calc deriv
        VecMulMtrx<N>(T, M, Tderiv);

        // calc Tn with Euler
        for(unsigned int i = 0; i < N; i++)
        {
            Tn[i] = T[i] + h * Tderiv[i]; 
        }

        // T = Tn
        memcpy(T, Tn, N * sizeof(double));

        if(useDFT)
        {
            output << t << '\t';
            for(unsigned int k = 0; k < N; k++)
            {
                std::complex<double> Tk = DFT(T, N, k); // 0 => eigen_1 hit; 1,2 => eigen_2 hit
                if(t == 0.)
                {
                    Tk0[k] = Tk;
                }
                else
                {
                    double test = std::abs(Tk / Tk0[k]);
                    double expect1 = std::exp(0 * t);
                    double expect2 = std::exp(-3 * y * t);

                    if(std::abs(test - expect1) < 0.001)
                    {
                        std::cout << "expected eigen_1 hit: " << k << ' ' << test << std::endl;
                    }
                    if(std::abs(test - expect2) < 0.001)
                    {
                        std::cout << "expected eigen_2 hit: " << k << ' ' << test << std::endl;
                    }
                }
                output << std::abs(Tk) << '\t';
            }
            output << std::endl;
        }
        else
        {
            // print current T to out.txt
            output << t << '\t';
            for(unsigned int i = 0; i < N; i++)
            {
                output << T[i] << '\t';
            }
            output << std::endl;
        }
    }

}

/**
 * @brief start the temperature simulation (Runge Kutta 2 version) 
 * 
 * @tparam N the number of coffee mugs
 * @tparam useDFT flag to calculate the discrete fourier transformation
 * @param T the input temperatures
 * @param M the matrix
 * @param Tend the time, where the simulation ends
 * @param h the step width
 * @param y the y coefficient
 * @param output the output stream for printing the results
 */
template<unsigned int N, bool useDFT> void tempSimRK2(double T[N], double M[N][N], const double Tend, const double h, const double y, std::ofstream& output)
{
    double Tderiv[N] = {0.}; // deriv T
    double Tk1[N] = {0.}; // k1 values
    double Tk2[N] = {0.}; // k2 values
    double Tn[N] = {0.}; // next T
    std::complex<double> Tk0[N]; // Tk(0)

    for(double t = 0.; t < Tend; t += h)
    {
        // calc deriv
        VecMulMtrx<N>(T, M, Tderiv);

        // Tk1 = h * Tderiv
        for(unsigned int i = 0; i < N; i++)
        {
            Tk1[i] = h * Tderiv[i]; 
        }

        // update T to (T + Tk1 / 2)
        for(unsigned int i = 0; i < N; i++)
        {
            T[i] += (Tk1[i] / 2.); 
        }

        // calc deriv of new T
        VecMulMtrx<N>(T, M, Tderiv);

        // Tk2 = h * Tderiv
        for(unsigned int i = 0; i < N; i++)
        {
            Tk2[i] = h * Tderiv[i]; 
        }

        // Tn = T + Tk2
        for(unsigned int i = 0; i < N; i++)
        {
            Tn[i] = T[i] + Tk2[i]; 
        }

        // T = Tn
        memcpy(T, Tn, N * sizeof(double));

        if(useDFT)
        {
            output << t << '\t';
            for(unsigned int k = 0; k < N; k++)
            {
                std::complex<double> Tk = DFT(T, N, k); // 0 => eigen_1 hit; 1,2 => eigen_2 hit
                if(t == 0.)
                {
                    Tk0[k] = Tk;
                }
                else
                {
                    double test = std::abs(Tk / Tk0[k]);
                    double expect1 = std::exp(0 * t);
                    double expect2 = std::exp(-3 * y * t);

                    if(std::abs(test - expect1) < 0.001)
                    {
                        std::cout << "expected eigen_1 hit: " << k << ' ' << test << std::endl;
                    }
                    if(std::abs(test - expect2) < 0.001)
                    {
                        std::cout << "expected eigen_2 hit: " << k << ' ' << test << std::endl;
                    }
                }
                output << std::abs(Tk) << '\t';
            }
            output << std::endl;
        }
        else
        {
            // print current T to out.txt
            output << t << '\t';
            for(unsigned int i = 0; i < N; i++)
            {
                output << T[i] << '\t';
            }
            output << std::endl;
        }
    }

}

int main(int argc, char const *argv[])
{
    std::ofstream output;
    output.open("out.txt");

    if(argv[1][0] == '1') // Task 1
    {
        const unsigned int N = 3; // Number of coffee mugs
        const double h = 0.01; // step width
        const double Tend = 5.; // simulation time
        const double y = 1.;

        double T[N] = {0., 1., 0.}; // current T
        double M[N][N] = {
                                {-y,      y,  0},
                                { y, -2 * y,  y},
                                { 0,      y, -y}
                            };

        if(argv[1][1] == 'a')
        {
            tempSim<N, false>(T, M, Tend, h, y, output);
        }
        else if(argv[1][1] == 'b')
        {
            tempSim_exact(Tend, h, y, output);
        }
    }
    else if(argv[1][0] == '2') // Task 2
    {
        const unsigned int N = 3; // Number of coffee mugs
        const double h = 0.01; // step width
        const double Tend = 5.; // simulation time
        const double y = 1.;

        double T[N] = {0., 1., 0.}; // current T
        double M[N][N];
        createM<N>(M, y);

        if(argv[1][1] == 'a')
        {
            tempSim<N, false>(T, M, Tend, h, y, output);
        }
        else if(argv[1][1] == 'b')
        {
            tempSim<N, true>(T, M, Tend, h, y, output);
        }
    }
    else if(argv[1][0] == '3') // Task 3
    {
        const unsigned int N = 100; // Number of coffee mugs
        const double h = 0.1; // step width
        const double Tend = 32.; // simulation time
        const double y = 1.;

        double T[N] = {0.}; // current T
        T[50] = 1.;
        auto M = new double[N][N]; // big N expands over stack size => heap allocation
        createM<N>(M, y);

        tempSim<N, false>(T, M, Tend, h, y, output);

        delete[] M; // free matrix memory

        std::ofstream output2;
        output2.open("out2.txt");
        for(unsigned int i = 0; i < N; i++)
        {
            output2 << i << '\t' << T[i] << std::endl;
        }
        output2.close();
    }

    output.close();
    return 0;
}