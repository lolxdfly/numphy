\section{Problemstellung} \label{chap:problem}
Gegeben seien drei Kaffeetassen mit Temperaturen $T_1$, $T_2$ und
$T_3$, die jeweils im Wärmeaustausch mit ihren Nachbartassen stehen.
Die Temperaturänderung jeder Tasse ist proportional zur Temperaturdifferenz
zu den Nachbartassen und beschrieben durch das Gleichungssystem:
\begin{equation}
    \begin{gathered}
        \dot{T_1} = \gamma(T_2 - T_1)\\
        \dot{T_2} = -\gamma(T_2 - T_1) + \gamma(T_3 - T_2)\\
        \dot{T_3} = -\gamma(T_3 - T_2)
    \end{gathered} \label{math:simpForm}
\end{equation}
oder in Matrixschreibweise als
\begin{equation}
    \begin{gathered}
        \begin{pmatrix}
            \dot{T_1} \\ 
            \dot{T_2} \\
            \dot{T_3}
        \end{pmatrix} =
        \begin{pmatrix}
            -\gamma & \gamma & 0\\ 
            \gamma & -2\gamma & \gamma\\
            0 & \gamma & -\gamma
        \end{pmatrix}
        \begin{pmatrix}
            T_1 \\ 
            T_2 \\
            T_3
        \end{pmatrix}
    \end{gathered} \label{math:mtrxForm}
\end{equation}

\section{Aufgabenstellung} \label{chap:task}
In diesem Kapitel werden die drei Aufgabenstellungen vorgestellt.

\subsection{Teil 1: Zeitentwicklung mit Euler- oder Runge-Kutta-Verfahren} \label{sec:task1}
Berechnen Sie mit Euler- oder Runge-Kutta-Verfahren die zeitliche Entwicklung der Temperaturen $T_l(t)$ der Kaffeetassen
für $\gamma = 1$ ausgehend bei der Startbedingung $T_1(0) = T_3(0) = 0$ und $T_2(0) = 1$. Vergleichen Sie das Ergebnis in
einem Plot mit der exakten Lösung aus der Vorlesung.\\
\subsubsection*{Exakte Lösung aus der Vorlesung}
\begin{equation}
    \begin{gathered}
        \begin{pmatrix}
            T_1 \\ 
            T_2 \\
            T_3
        \end{pmatrix} =
        \begin{pmatrix}
            \frac{1}{3} - \frac{1}{3} e^{-3\gamma t} \\ 
            \frac{1}{3} + \frac{1}{3} e^{-3\gamma t} \\
            \frac{1}{3} - \frac{1}{3} e^{-3\gamma t}
        \end{pmatrix}
    \end{gathered} \label{math:exact}
\end{equation}

\subsection{Teil 2: Diskrete Fourier-Transformation des Temperaturprofils} \label{sec:task2}
Das Kaffeetassensystem wird auf periodische Randbedingungen erweitert, d.h. $T_1$ wird zusätzlich an $T_3$ gekoppelt.
Das Gleichungssystem ändert sich damit zu:
\begin{equation}
    \begin{gathered}
        \begin{pmatrix}
            \dot{T_1} \\ 
            \dot{T_2} \\
            \dot{T_3}
        \end{pmatrix} =
        \begin{pmatrix}
            -2\gamma & \gamma & \gamma\\ 
            \gamma & -2\gamma & \gamma\\
            \gamma & \gamma & -2\gamma
        \end{pmatrix}
        \begin{pmatrix}
            T_1 \\ 
            T_2 \\
            T_3
        \end{pmatrix}
    \end{gathered} \label{math:linked}
\end{equation}
Berechnen Sie auch hier den zeitlichen Verlauf der Temperaturen $T_l(t)$ sowie den der diskreten Fourier-Transformation ($N = 3$)
\begin{equation}
    \begin{gathered}
        T_k(t) = \frac{1}{\sqrt{N}} \sum\limits_{l=0}^N e^{\frac{2\pi kl}{N}i} T_l(t) \quad\text{mit}\quad k = 0, \dots, N - 1
    \end{gathered} \label{math:dft}
\end{equation}
Vergleichen Sie den Verlauf der $T_k(t)$ mit den Eigenwerten $\lambda_i$ (welche sind das?) der Matrix. Prüfen Sie dazu
$T_k(t)/T_k(0) = e^{\lambda t}$.

\subsection{Teil 3: Erweiterung auf 100 Kaffeetassen} \label{sec:task3}
Das obige Gleichungssystem lässt sich ganz einfach auf $N$ gekoppelte Kaffeetassen erweitern mit
\begin{equation}
    \begin{gathered}
        \begin{pmatrix}
            \dot{T_1} \\ 
            \dot{T_2} \\
            \dot{T_3} \\
            \vdots \\
            \\
            \dot{T_N}
        \end{pmatrix} =
        \begin{pmatrix}
            -2\gamma & \gamma & 0 & \cdots & 0 & \gamma \\ 
            \gamma & -2\gamma & \gamma & \ddots & & 0\\
            0 & \gamma & \ddots & \ddots & \ddots & \vdots \\
            \vdots & \ddots & \ddots & \ddots & \gamma & 0 \\
            0 & & \ddots & \gamma & -2\gamma & \gamma \\
            \gamma & 0 & \cdots & 0 & \gamma & -2\gamma
        \end{pmatrix}
        \begin{pmatrix}
            T_1 \\ 
            T_2 \\
            T_3 \\
            \vdots \\
            \\
            T_N
        \end{pmatrix}
    \end{gathered} \label{math:bigForm}
\end{equation}
Erweitern Sie Ihr Programm auf 100 Kaffeetassen und ermitteln Sie $T_i(t)$ für die Startbedingung
\begin{equation}
    \begin{gathered}
        T_l(0) =
        \begin{cases} 
            1, & l = 50 \\
            0, & l \neq 50 
        \end{cases}
    \end{gathered} \label{math:startTemp}
\end{equation}
mit $\gamma = 1$. Fitten Sie eine Lorentz- und eine Gaußkurve an das Wärmeprofil $T_l(t)$ zum Zeitpunkt $t = 32$.
Welche Fitfunktion passt besser? Bestimmen Sie die Halbwertsbreite bzw. Varianz der Temperaturverteilung aus dem Fit.

\section{Umsetzung} \label{chap:imp}
In diesem Kapitel geht es um die Umsetzung der verschiedenen Teilaufgaben. Dabei wird zunächst der C++ Code
erläutert. Anschließend werden Gnuplot-Skripte und das Makefile erklärt. Der vollständige Code, inklusive Skripte
ist in Anhang \ref{apx:code} zu finden.

\subsection{C++ Code} \label{sec:cpp}
Um den Code später einfach für mehrere Kaffeetassen erweitern zu können, wird die Berechnung durch
Vektor-Matrix-Multiplikation gewählt. Dafür wird eine Funktion \textit{VecMulMtrx(double vecIn[N], double mtrx[N][N], double vecOut[N])}
(s. Zeile 16) erstellt, wobei sich auf quadratisch große Matrizen der Größe $N$ eingeschränkt werden durfte.
\textit{vecIn} wird mit \textit{mtrx} multipliziert und das Ergebnis wird in \textit{vecOut} gespeichert.\\
Eine weitere Hilfsfunktion namens \textit{createM(double mtrx[N][N], double y)} (s. Zeile 38) erstellt die in Formel
\ref{math:linked} und \ref{math:bigForm} verwendete Matrix für $N$ Kaffeetassen. \textit{mtrx} ist die zu befüllende
Matrix und \textit{y} ist der $\gamma$ Faktor aus den Formeln.\\
Die Funktion \textit{DFT(double T[], unsigned int N, unsigned int k)} (s. Zeile 72) berechnet die diskrete 
Fourier-Transformation aus Formel \ref{math:dft}. \textit{T} entspricht dabei $T_l$. Alle anderen Parameter haben
einen identischen Bezeichner, wie in der Formel. Als Rückgabewert liefert \textit{DFT} den komplexen Wert.\\
Für Aufgabenteil \ref{sec:task1} wird die exakte Lösung aus der Vorlesung benötigt (s. Formel \ref{math:exact}).
Damit später mit der exakten Lösung verglichen werden kann wird die Funktion
\textit{tempSim\_exact(const double Tend, const double h, const double y, std::ofstream\& output)} (s. Zeile 104)
erstellt. Die exakte Lösung wird hierbei von $t = 0$ bis $t = \textit{Tend}$ mit der Schrittbreite \textit{h}
berechnet. \textit{y} entspricht dem $\gamma$ Faktor und \textit{output} ist ein Textstream, welcher die Temperaturen
zu jedem Zeitpunkt in eine \textit{out.txt} schreibt. Der Verlauf der Temperaturen kann später anhand der \textit{out.txt}
mit Gnuplot angezeigt werden.\\
Die Temperatursimulation aller Aufgabenteile werden in einer Funktion namens 
\textit{tempSim(double T[N], double M[N][N], const double Tend, const double h, const double y, std::ofstream\& output)} (s. Zeile 145)
zusammengefasst. Dabei beschreiben die Templateparameter \textit{N} die Anzahl der Tassen und \textit{useDFT} steuert,
ob für Aufgabenteil \ref{sec:task2} die diskrete Fourier-Transformation auf die Temperaturen angewendet wird. Bei den
normalen Funktionsparametern steht \textit{T} für die Anfangstemperaturen. Da die Parameterübergabe von \textit{T} als
Zeiger erfolgt, ändert sich auch die Werte des übergebenen Parameters. Dadurch befinden sich am Ende der Simulation
auch die Temperaturen der letzten Iteration im übergebenen Parameter. \textit{M} beschreibt die Korrelationsmatrix
(s. \textit{createM}) und \textit{Tend}, \textit{h}, \textit{y} und \textit{output} haben die gleiche Funktion wie bei
der \textit{tempSim\_exact} Funktion. Die Funktion \textit{tempSim} verwendet das Euler Verfahren für die Berechnung
der Temperaturen. Das Runge-Kutta-2 Verfahren ist in der ansonsten gleichen Funktion 
\textit{tempSimRK2(double T[N], double M[N][N], const double Tend, const double h, const double y, std::ofstream\& output)} (s. Zeile 220)
implementiert.\\
Zuletzt gibt es noch die \textit{main(int argc, char const *argv[])} (s. Zeile 306) Funktion des Programmes, welche
den Einstiegspunkt darstellt. Hier werden Filestreams für Ausgaben erstellt und Konstanten werden festgelegt. Damit
das Programm nicht für die unterschiedlichen Aufgabenteile neu kompiliert werden muss, kann anhand der Startparameter
ein Aufgabenteil ausgewählt werden. So kann z.B. durch \textit{./tempSim 1a} der erste Teil von Aufgabe 1 berechnet werden
und mit \textit{./tempSim 2b} lässt sich der zweite Teil von Aufgabe 2 berechnen.\\

\subsection{Gnuplot und Makefile} \label{sec:gnumake}
Für das Generieren von Diagrammen wurden insgesamt zwei Gnuplot-Skripte erstellt. Das erste Skript \textit{plot.plg}
plottet einfach die zweite, dritte und vierte Spalte über der ersten Spalte. Damit lassen sich drei Temperaturverläufe
über die Zeit anzeigen. Das zweite Gnuplot-Skript fittet eine Lorentz- und eine Gaußkurve an die Temperaturverteilung
aus \textit{out2.txt}. Dabei wurde
\begin{equation}
    \begin{gathered}
        G(x)= \frac{1}{\sigma \sqrt{2\pi}} exp(- \frac{(x - \mu)^2}{2\sigma^2})
    \end{gathered} \label{math:gauss}
\end{equation}
als Definition für die Gaußkurve verwendet und
\begin{equation}
    \begin{gathered}
        L(x)=\frac{\frac{\Gamma}{2}}{\pi((x - \mu)^2 + (\frac{\Gamma}{2})^2)}
    \end{gathered} \label{math:lorentz}
\end{equation}
als Definition für die Lorentzkurve.\\
Das Makefile erleichtert das Kompilieren des Codes und das Ausführen des Programmes inklusive der Diagrammgenerierung.
Das \textit{tempSim} Taret kompiliert die main.cpp mit allen Optimierungen angeschaltet (\textit{-O3}). Des Weiteren
gibt es Targets für jeden Aufgabenteil, welche ein aktuell kompiliertes Programm sicherstellen und den entsprechenden
Plot mit \textit{gnuplot} ausführen. Außerdem gibt es ein \textit{clean} Target, welches das kompilierte Programm und
alle Ausgabedaten (\textit{out.txt} und \textit{out2.txt}) entfernt.

\section{Ergebnisse} \label{chap:res}
In diesem Kapitel werden die Ergebnisse der einzelnen Teilaufgaben vorgestellt.

\subsection{Teil 1} \label{sec:res1}
Die zeitliche Entwicklung der Temperaturen für $\gamma = 1$ kann durch den Befehl \textit{make plot1a}
generiert und visualisiert werden (s. Abbildung \ref{fig:res1a}). Die Simulation wurde für den Zeitintervall $[0, 5]$ mit einer
Simulationsschrittweite von $\textit{h} = 0.01$ durchgeführt. Zu sehen ist, dass die Temperatur $T_2$ von $1 K$
exponentiell abnimmt, bis sie sich bei ca. $t = 2$ mit den anderen Temperaturen bei $0.33 K$ trifft. $T_1$ und $T_3$
steigen exponentiell von $0 K$ auf $0.33 K$.
\begin{figure}[H]
    \includegraphics[width=0.75\textwidth]{res1a.pdf}
    \centering
    \caption{Simulierte Temperaturverläufe von Aufgabenteil \ref{sec:task1}}
    \label{fig:res1a}
\end{figure}
Der Temperaturverlauf der exakten Lösung aus der Verlesung wird mit
\textit{make plot1b} berechnet und ist in Abbildung \ref{fig:res1b} zu sehen. Es wurde mit denselben Parametern
gerechnet wie im ersten Teil der Aufgabe. Ein Unterschied zu der approximierten Lösung ist fast nicht erkennbar.
\begin{figure}[H]
    \includegraphics[width=0.75\textwidth]{res1b.pdf}
    \centering
    \caption{Exakte Temperaturverläufe von Aufgabenteil \ref{sec:task1}}
    \label{fig:res1b}
\end{figure}

\subsection{Teil 2} \label{sec:res2}
Mit \textit{make plot2a} werden die Temperaturverläufe mit der neuen Matrix (s. Formel \ref{math:linked}) berechnet
und visualisiert (s. Abbildung \ref{fig:res2a}). Hierbei ist kein Unterschied zu der Berechnung aus Aufgabenteil \ref{sec:task1}
zu erkennen. Das lässt sich durch die Symmetrie der Instanz erklären. Die Startinstanz des Problems hat eine Temperatur von
$1 K$ genau in der geometrischen Mitte des Versuchsaufbaus. Die Temperatur wird sich von dort aus gleichmäßig auf
anliegende Tassen ausbreiten. Dabei spielt es keine Rolle, ob die Randtassen sich gegenseitig aufwärmen oder abkühlen,
da sie immer dieselbe Temperatur haben.
\begin{figure}[H]
    \includegraphics[width=0.75\textwidth]{res2a.pdf}
    \centering
    \caption{Simulierte Temperaturverläufe von Aufgabenteil \ref{sec:task2}}
    \label{fig:res2a}
\end{figure}
In dem zweiten Teil dieser Aufgabe sollte die diskrete Fourier-Transformation
auf den Temperaturzustand angewendet werden. Mithilfe von \textit{make plot2b} kann dies berechnet werden. Die dabei
entstehende Abbildung \ref{fig:res2b} zeigt den Verlauf der transformierten Temperaturzustände. Hierbei wurde jeweils
die Länge des komplexen Vektors verwendet. Außerdem wird der Verlauf der $T_k(t)$ mit den Eigenwerten $\lambda_i$ der
Matrix verglichen. Dafür wird $T_k(t)/T_k(0) - e^{\lambda t}$ für jeden Zeitschritt berechnet und mit $0.001$
verglichen. Dadurch kann geprüft werden, ob die Werte hinreichend gleich sind. Wenn dies der Fall ist, wird eine
Ausgabe des $T_k$s mit dem zugehörigen $k$ getätigt. Die Eigenwerte der Matrix lauten: $\lambda_1 = 0$ und $\lambda_2 = -3\gamma$
(Herleitung siehe Anhang \ref{apx:eigen}). Für $k = 0$ hat $T_k(t)/T_k(0)$ den Wert $e^{\lambda_1 t}$ und für $k = 1$ oder
$k = 2$ entspricht $T_k(t)/T_k(0)$ dem Wert $e^{\lambda_2 t}$.
\begin{figure}[H]
    \includegraphics[width=0.75\textwidth]{res2b.pdf}
    \centering
    \caption{Verlauf der fourier-transformierten Temperaturzustände von Aufgabenteil \ref{sec:task2}}
    \label{fig:res2b}
\end{figure}

\subsection{Teil 3} \label{sec:res3}
Durch den Befehl \textit{make plot3} wird die Simulation für $N = 100$ und $\textit{Tend} = 32$ Kaffeetassen
durchgeführt. \textit{N} und \textit{Tend} sind dabei normalere Parameter und können im Prinzip beliebig groß gewählt
werden, solange der Rechner genügend Ressourcen zur Verfügung stellt. In Abbildung \ref{fig:res3} sind die 
Temperaturverläufe der ersten drei Tassen zu sehen. Da auch bei dieser Aufgabenstellung die Startinstanz wieder eine
Tasse mit Temperatur $1 K$ genau in der geometrischen Mitte des Aufbaus ist, breitet sich die Temperatur über die Zeit
in Richtung Rand\footnote{Der Rand ist kein echter Rand, sondern verbindet wieder erste und letzte Tasse.} der Tassen
aus. In der Abbildung erkennt man dies, weil dort zuerst die dritte Tasse ein Temperaturanstieg verzeichnet. Zeitlich
kurz darauf folgen die Temperaturanstiege zunächst von der zweiten und dann von der ersten Tasse.
\begin{figure}[H]
    \includegraphics[width=0.75\textwidth]{res3.pdf}
    \centering
    \caption{Simulierte Temperaturverläufe von Aufgabenteil \ref{sec:task3}}
    \label{fig:res3}
\end{figure}
Mit \textit{make plot3Fit} wird zusätzlich der letzte Temperaturstand (bei $t = 32$) geplottet (s. Abbildung \ref{fig:res3Fit}). Außerdem werden
Lorentz- und Gaußkurve (s. Formel \ref{math:lorentz} und \ref{math:gauss}) an den Verlauf gefittet und angezeigt.
Zu sehen ist, dass die Gaußkurve ziemlich genau auf den geplotteten Punkten verläuft, während die Lorentzkurve
versetzt ist. Die exakten Parameter und deren asymptotischer Standardfehler sind in den Tabellen \ref{tab:lorentzParam}
und \ref{tab:gaussParam} zu sehen. Dabei fällt auf, dass der Fehler bei der Lorentzkurve deutlich höher ist, als der
Fehler der Gaußkurve. Die Halbwertsbreite $\Gamma$ der Lorentzkurve entspricht $12.19$ und die Varianz $\sigma^2$
hat den Wert $63.92$.
\begin{figure}[H]
    \includegraphics[width=0.75\textwidth]{res3Fit.pdf}
    \centering
    \caption{Temperaturstand bei $t = 32$ mit gefitteter Lorentz- und Gaußkurve}
    \label{fig:res3Fit}
\end{figure}
\begin{table}[H]
    \centering
    \begin{tabular}{|c|c|c|} 
        \hline
        Parameter & Wert & Asymptotischer Standardfehler \\ \hline
        $\Gamma$ & $12.19$ & $3.832\%$ \\ \hline
        $\mu$ & $50.01$ & $0.4665\%$ \\ \hline
    \end{tabular}
    \caption{Parameter und deren Asymptotischer Standardfehler der Lorentzkurve}
    \label{tab:lorentzParam}
\end{table}
\begin{table}[H]
    \centering
    \begin{tabular}{|c|c|c|} 
        \hline
        Parameter & Wert & Asymptotischer Standardfehler \\ \hline
        $\sigma$ & $7.99$ & $0.004213\%$ \\ \hline
        $\mu$ & $50.0$ & $0.0008245\%$ \\ \hline
    \end{tabular}
    \caption{Parameter und deren Asymptotischer Standardfehler der Gaußkurve}
    \label{tab:gaussParam}
\end{table}

\newpage
\appendix
\section{Eigenwertbestimmung}
\label{apx:eigen}
\begin{equation}
    A =
    \begin{pmatrix}
        -2\gamma  & \gamma & \gamma\\ 
        \gamma & -2\gamma & \gamma\\
        \gamma & \gamma & -2\gamma
    \end{pmatrix}
\end{equation}
Berechnung der Eigenwerte durch das charakteristische Polynom:
\begin{gather*}
    |A - \lambda E| = 0 \\
    \begin{vmatrix}
        -2\gamma - \lambda  & \gamma & \gamma\\ 
        \gamma & -2\gamma - \lambda & \gamma\\
        \gamma & \gamma & -2\gamma - \lambda
    \end{vmatrix} = 0 \\
    (-2\gamma - \lambda)(-2\gamma - \lambda)(-2\gamma - \lambda) +
    \gamma\gamma\gamma + \gamma\gamma\gamma - 
    \gamma(-2\gamma - \lambda)\gamma - \gamma(-2\gamma - \lambda)\gamma
    -\gamma(-2\gamma - \lambda)\gamma = 0 \\
    (-2\gamma - \lambda)(-2\gamma - \lambda)(-2\gamma - \lambda) +
    2\gamma^3 - 
    3\gamma^2(-2\gamma - \lambda) = 0 \\
    (4\gamma^2 + 4\gamma\lambda + \lambda^2)(-2\gamma - \lambda) +
    2\gamma^3 - 
    (-6\gamma^3 - 3\lambda\gamma^2) = 0 \\
    (-8\gamma^3 - 8\lambda\gamma^2 - 2\gamma\lambda^2) - (4\lambda\gamma^2 + 4\gamma\lambda^2 + \lambda^3) +
    2\gamma^3 +
    6\gamma^3 + 3\lambda\gamma^2 = 0 \\
    -8\gamma^3 - 8\lambda\gamma^2 - 2\gamma\lambda^2 - 4\lambda\gamma^2 - 4\gamma\lambda^2 - \lambda^3 +
    8\gamma^3 + 3\lambda\gamma^2 = 0 \\
    -9\lambda\gamma^2 - 6\gamma\lambda^2 - \lambda^3 = 0 \\
    \lambda(-9\gamma^2 - 6\gamma\lambda - \lambda^2) = 0 \\
\end{gather*}
\begin{multicols}{2}
    \begin{gather*}
        \lambda_1 = 0 \\
    \end{gather*}
\columnbreak
    \begin{gather*}
        -9\gamma^2 - 6\gamma\lambda - \lambda^2 = 0 \\
        \lambda^2 + 6\gamma\lambda + 9\gamma^2 = 0 \\
        -\frac{6\gamma}{2} \pm \sqrt{\frac{(6\gamma)^2}{4} - 9\gamma^2} = \lambda_{2,3} \\
        -3\gamma \pm \sqrt{9\gamma^2 - 9\gamma^2} = \lambda_{2,3} \\
        -3\gamma \pm \sqrt{0} = \lambda_{2,3} \\
        -3\gamma = \lambda_{2,3} \\
        \lambda_2 = -3\gamma \\
    \end{gather*}
\end{multicols}

\section{Code}
\label{apx:code}
\lstinputlisting{../main.cpp}
\lstinputlisting{../plot.plg}
\lstinputlisting{../plotFit.plg}
\lstinputlisting{../Makefile}