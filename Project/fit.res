

*******************************************************************************
Sun Mar 21 14:19:29 2021


FIT:    data read from "out2.txt" u 1:2
        format = x:z
        #datapoints = 100
        residuals are weighted equally (unit weight)

function used for fitting: G(x)
	G(x)=(1 / (o * sqrt(2 * pi))) * exp(- ((x - u)**2) / (2 * o**2))
fitted parameters initialized with current variable values

iter      chisq       delta/lim  lambda   o             u            
   0 3.1446496329e-01   0.00e+00  3.97e-02    1.000000e+00   1.000000e+00
  49 4.5967771204e-09  -2.22e-02  3.97e-10    7.994766e+00   5.000000e+01

After 49 iterations the fit converged.
final sum of squares of residuals : 4.59678e-09
rel. change during last iteration : -2.22319e-07

degrees of freedom    (FIT_NDF)                        : 98
rms of residuals      (FIT_STDFIT) = sqrt(WSSR/ndf)    : 6.84879e-06
variance of residuals (reduced chisquare) = WSSR/ndf   : 4.69059e-11

Final set of parameters            Asymptotic Standard Error
=======================            ==========================
o               = 7.99477          +/- 0.0003368    (0.004213%)
u               = 50               +/- 0.0004122    (0.0008245%)

correlation matrix of the fit parameters:
                o      u      
o               1.000 
u              -0.004  1.000 


*******************************************************************************
Sun Mar 21 14:19:29 2021


FIT:    data read from "out2.txt" u 1:2
        format = x:z
        #datapoints = 100
        residuals are weighted equally (unit weight)

function used for fitting: L(x)
	L(x)=(1 / pi) * ((0.5 * R) / ((x - a)**2 + (0.5 * R)**2))
fitted parameters initialized with current variable values

iter      chisq       delta/lim  lambda   R             a            
   0 4.7472176883e-01   0.00e+00  5.01e-02    1.000000e+00   1.000000e+00
  37 1.8722354757e-03  -5.28e-01  5.01e-09    1.219445e+01   5.000868e+01

After 37 iterations the fit converged.
final sum of squares of residuals : 0.00187224
rel. change during last iteration : -5.27712e-06

degrees of freedom    (FIT_NDF)                        : 98
rms of residuals      (FIT_STDFIT) = sqrt(WSSR/ndf)    : 0.00437086
variance of residuals (reduced chisquare) = WSSR/ndf   : 1.91044e-05

Final set of parameters            Asymptotic Standard Error
=======================            ==========================
R               = 12.1945          +/- 0.4673       (3.832%)
a               = 50.0087          +/- 0.2333       (0.4665%)

correlation matrix of the fit parameters:
                R      a      
R               1.000 
a              -0.006  1.000 
