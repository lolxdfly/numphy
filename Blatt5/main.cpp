#include <iostream>
#include <fstream>
#include <math.h>
#include <functional>

double centDiff(std::function<double(double)> f, double x, double h)
{
    return (f(x + h) - f(x - h)) / (2 * h);
}

void calcDeriv(std::function<double(double)> f, std::pair<double, double> range, double h, std::ofstream& os)
{
    // calc the derivation
    for(double x = range.first; x <= range.second; x += h)
    {
        double dfx = centDiff(f, x, h);

        // print to console and output file
        //std::cout << "f'(" << x << ") = " << dfx << std::endl;
        os << x << '\t' << f(x) << '\t' << dfx << std::endl;
    }
}

void calcErr(std::function<double(double)> f, std::function<double(double)> fDerif, double x, std::pair<double, double> range, double h, std::ofstream& os)
{
    // calc the derivation
    for(double hi = range.first; hi <= range.second; hi += h)
    {
        double dfx = centDiff(f, x, hi);

        // print to console and output file
        //std::cout << "f'(" << x << ") = " << dfx << std::endl;
        os << hi << '\t' << (dfx - fDerif(x)) << std::endl;
    }
}

double ftest(double x)
{
    return x * x;
}
double f(double x)
{
    return std::exp(- x / 2) * std::cos(2 * x);
}
double f_(double x)
{
    return - 0.5 * std::exp(- x / 2) * std::cos(2 * x) + std::exp(- x / 2) * - 2 * std::sin(2 * x);
}
double g(double x)
{
    return std::exp(- 1 / (2 * x)) * std::sin(x);
}
double h(double x)
{
    return std::exp(- 1 / (x * x)) * std::sin(0.2 * x * x);
}

int main(int argc, char const *argv[])
{
    // open output stream
    std::ofstream output;
    output.open("out.txt");

    // config
    const double step_width = 0.1;

    if(argc > 1)
    {
        // calc fh'-f' and print vales to output stream 
        calcErr(f, f_, 4, std::make_pair(0.001, 1), 0.01, output);
    }
    else
    {
        // calc f' and print vales to output stream 
        calcDeriv(f, std::make_pair(-10., 2.), step_width, output);
        //calcDeriv(g, std::make_pair(-10., 10.), step_width, output);
        //calcDeriv(h, std::make_pair(-5., 20.), step_width, output);
    }
    
    // close output stream and exit
    output.close();
    return 0;
}
